<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webmppe
 */
namespace webmppe;
?>

<!doctype html>
<html lang="es">
<head>	

	<?php wp_head(); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php the_title( ' ' ,  '-MPPE'); ?></title>
</head>
<div class="" id="cintillo"><?php dynamic_sidebar( 'cintillo' ); ?></div>
<div id="Menuheader" class="row">
	<div class="col-2 col-lg-2 col-md-2 col-xs-2 wow zoomInDown" data-wow-duration="2s" style="visibility: visible; animation-duration:2s; animation-delay: 1000ms; animation-iteration-count: 1; animation-name: zoomInDown;">
	<?php if ( has_custom_logo() && ! $show_title ) : ?>
		<div class="site-logo pulse animated " data-wow-delay="4s" data-wow-iteration="infinite" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-delay: 4s; animation-iteration-count: infinite; animation-name: pulse;"><h1>	<?php the_custom_logo(); ?></h1></div> <!--Muestra el Logo de la Página-->
	<?php endif; ?></div>
	<div class="col-10 col-lg-10 col-md-10 col-xs-10"> <?php wp_nav_menu( array( 'theme_location' => 'Menú header' ) ); ?></div>
</div>
<div id="menuprincipal">
  				<?php  wp_nav_menu( array( 'theme_location' => 'Menú principal' ) ); ?>
</div>	

<!--<div id="menuprincipal" class=""><?php  // get_template_part( 'template-parts/menu' ); ?></div>-->
<script>
        new WOW().init();
</script>


<body>
	

