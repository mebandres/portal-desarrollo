<?php get_header(); ?>

<div class="mihome">


	<article>

	<div class="title"><h1> Noticias  </h1></div>


			<?php if (have_posts()) : ?>


			<div id="misCategorias">
				
				<p>
					<?php $this_category = get_category($cat); ?>
					<!-- Si la categoría es padre, listar categoría -->
					<?php if ($this_category->category_parent == 0) { ?>
					
					<?php $this_category->category_parent = $cat; ?>
					<?php } else { ?> 
					<!-- Si la categoría no es padre, listar la categoría padre -->
					<?php $parent_category = get_category($this_category->category_parent); ?>
					
						<a href="<?php echo get_category_link($parent_category->cat_ID); ?>" title="<?php echo $parent_category->cat_name; ?>">
						<?php echo $parent_category->cat_name; ?></a>
						<?php } ?>

					<?php endif; ?>
						   
								<?php 
								wp_list_cats('list=1&use_desc_for_title=0&child_of=' . $this_category->category_parent); ?>
				</p>		

			 </div>


    </article>


<!--<h1>Soy home.php</h1>-->

<?php get_footer(); ?>

</div>
