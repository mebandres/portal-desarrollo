<?php
/**
 * webmppe functions and definitions
 *
 * This file must be parseable by PHP 5.2.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package webmppe
 */
//
// Hoja de estilo Personal
// 
function my_CSS() {	
	wp_enqueue_style( 'style.css', get_stylesheet_directory_uri() . '/style.css');	
}
add_action( 'wp_enqueue_scripts', 'my_CSS' );
function main_CSS() {	
	wp_enqueue_style( 'main.css', get_stylesheet_directory_uri() . '/Componentes/css/main.css');	
}
//
// Incluir Fuentes 
// 
add_action( 'wp_enqueue_scripts', 'main_CSS' );
function fuentes_CSS() {	
	wp_enqueue_style( 'fuentes.css', get_stylesheet_directory_uri() . '/Componentes/css/fuentes.css');	
}
add_action( 'wp_enqueue_scripts', 'fuentes_CSS' );
//
// Incluir Jquery CSS
// 
function jquery_js() {	
	wp_enqueue_script( 'jquery-3.6.0.min.js', get_stylesheet_directory_uri() . '/Componentes/jquery/jquery-3.6.0.min.js');	
}
add_action( 'wp_enqueue_scripts', 'jquery_js' );

// Incluir Bootstrap JS
// 
//function bootstrap_bundle_script() {
//	wp_enqueue_script( 'bootstrap.bundle.min.js', get_stylesheet_directory_uri() . '/Componentes/bootstrap-5.0/js/bootstrap.bundle.min.js');
//}
//add_action( 'wp_enqueue_scripts', 'bootstrap_bundle_script' );
//
//
function pooper_script() {
	wp_enqueue_script( 'popper.min.js', get_stylesheet_directory_uri() . '/Componentes/bootstrap-5.0/js/popper.min.js');	
}
//
//
add_action( 'wp_enqueue_scripts', 'pooper_script' );
//
//
function bootstrap_script() {
	wp_enqueue_script( 'bootstrap.min.js', get_stylesheet_directory_uri() . '/Componentes/bootstrap-5.0/js/bootstrap.min.js');	
}
add_action( 'wp_enqueue_scripts', 'bootstrap_script' );
//
// Incluir Bootstrap CSS
//
function bootstrap_css() {
	wp_enqueue_style( 'bootstrap_css', get_stylesheet_directory_uri() . '/Componentes/bootstrap-5.0/css/bootstrap.min.css' ); 
	 
}
add_action( 'wp_enqueue_scripts', 'bootstrap_css' );
//
// Incluir Animate CSS
//
function animate_css() {
    wp_enqueue_style( 'animate_css', get_stylesheet_directory_uri() . '/Componentes/animate/animate.css' ); 
     
}
add_action( 'wp_enqueue_scripts', 'animate_css' );
//
// Incluir Iconos Bootstrap CSS
//
function bootstrapIcons_css() {
	wp_enqueue_style( 'bootstrapIcons_css', get_stylesheet_directory_uri() . '/Componentes/bootstrap-icons-1.4.1/bootstrap-icons.css' ); 
}
add_action( 'wp_enqueue_scripts', 'bootstrapIcons_css' );
//
// Incluir WOW
//
function wow_js() {  
    wp_enqueue_script( 'wow_js', get_stylesheet_directory_uri() . '/Componentes/wow/wow.min.js'); 
}
add_action( 'wp_enqueue_scripts', 'wow_js' );
//
// Incluir Iconos FontAwesome
//
function fontawesome_js() {  
    wp_enqueue_script( 'fontawesome-free_js', get_stylesheet_directory_uri() . '/Componentes/fontawesome-free-5.9.0-web/js/all.js'); 
}
add_action( 'wp_enqueue_scripts', 'fontawesome_js' );

function fontawesome_css() {
    wp_enqueue_style( 'fontawesome-free_css', get_stylesheet_directory_uri() . '/Componentes/fontawesome-free-5.9.0-web/css/all.css' ); 
}
add_action( 'wp_enqueue_scripts', 'fontawesome_css' );
//
// Publicando los Menus  de nuestra página
//
function register_my_menus_principal() {
  register_nav_menus(
    array(
      'Menú principal' => __( 'Menú principal' ),
      'id'            => '',
      'before_widget' => '<ul class="">',
      'after_widget'  => '</ul>',

    )
  );
}
add_action( 'init', 'register_my_menus_principal' );
//
// Publicando los Menus  de nuestra página
//
function register_my_menus_header() {
  register_nav_menus(
    array(

      'Menú header' => __( 'Menú header' )
     
    )
  );
}
add_action( 'init', 'register_my_menus_header' );
//
// Sorporte a Imágenes Miniaturas
//
add_theme_support( 'post-thumbnails' );


//
// Modificando las dimensiones de las Imagenes
//
add_image_size ( 'thumbnails_Slider', 4400, 2700, true );
//
//
// Soporte a las etiquetas HTML5
add_theme_support( 'post-formats',  array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
//
function theme_prefix_setup() {
	
	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-width' => true,
		'flex-height' => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );
//
// CREANDO LOS  MÓDULOS (widgets)
// Cintillo

function Cintillo() {
    register_sidebar( array(
        'name'          => 'Cintillo',
        'id'            => 'cintillo',
        'before_widget' => '<div class="row">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Cintillo' );
//
// Banner 02
//
function Banner02() {
    register_sidebar( array(
        'name'          => 'Banner02',
        'id'            => 'ban2',
        'before_widget' => '<div class="ban2">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner02' );
//
// Banner 03
//
function Banner03() {
    register_sidebar( array(
        'name'          => 'Banner03',
        'id'            => 'ban3',
        'before_widget' => '<div class="ban3">',
        'after_widget'  => '</div>',
    ) );
}
add_action( 'widgets_init', 'Banner03' );
//
// Banner 04
//
function Banner04() {
    register_sidebar( array(
        'name'          => 'Banner04',
        'id'            => 'ban4',
        'before_widget' => '<div class="ban4">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner04' );
//
// Banner 05
//
function Banner05() {
    register_sidebar( array(
        'name'          => 'Banner05',
        'id'            => 'ban5',
        'before_widget' => '<div class="ban5">',
        'after_widget'  => '</div>',
    ) );
}
//
// Banner 06
//
add_action( 'widgets_init', 'Banner05' );

function Banner06() {
    register_sidebar( array(
        'name'          => 'Banner06',
        'id'            => 'ban6',
        'before_widget' => '<div class="ban6">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner06' );


function Banner07() {
    register_sidebar( array(
        'name'          => 'Banner07',
        'id'            => 'ban7',
        'before_widget' => '<div class="ban7">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner07' );



function Banner08() {
    register_sidebar( array(
        'name'          => 'Banner08',
        'id'            => 'ban8',
        'before_widget' => '<div class="ban8">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner08' );



function Banner09() {
    register_sidebar( array(
        'name'          => 'Banner09',
        'id'            => 'ban9',
        'before_widget' => '<div class="ban9">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner09' );



function Banner10() {
    register_sidebar( array(
        'name'          => 'Banner10',
        'id'            => 'ban10',
        'before_widget' => '<div class="ban10">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner10' );



function Banner11() {
    register_sidebar( array(
        'name'          => 'Banner11',
        'id'            => 'ban11',
        'before_widget' => '<div class="ban11">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner11' );


function Banner12() {
    register_sidebar( array(
        'name'          => 'Banner12',
        'id'            => 'ban12',
        'before_widget' => '<div class="ban12">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner12' );


function Banner13() {
    register_sidebar( array(
        'name'          => 'Banner13',
        'id'            => 'ban13',
        'before_widget' => '<div class="ban13">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Banner13' );



















function Footer() {
    register_sidebar( array(
        'name'          => 'Footer',
        'id'            => 'copyright',
        'before_widget' => '<div class="copyright">',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'Footer' );

