<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<article class="<?php post_class();?>">

				<header class="title"> <h1><?php the_title(); ?></h1></header>

				<div>	
					<?php the_content(); ?> 
				</div>

		</article>

	<?php endwhile; else: ?>

		<article>
			<p>No hay contenido a mostrar </p>
		</article>

    <?php endif; ?>
    