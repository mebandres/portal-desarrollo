<div class="title"><h1 ><?php // single_cat_title(); ?></h1></div>

<!--Grid row-->
<div class="row wow fadeIn">
<?php
if ( have_posts() ) {
$counter = 1;
while ( have_posts() ) {
the_post();
?>

    <!--Grid column-->
    <div class=" col-4 col-xs-4   col-sm-4 col-md-4   col-lg-4   col-xl-4   col-xxl-4">
        <!--Featured image-->
        <div class="view overlay hm-white-slight rounded z-depth-2 mb-4">
		<a href="<?php echo get_permalink() ?>" class=""><?php the_post_thumbnail( 'medium-large', array( 'class'=> 'img-fluid')); ?></a>
            <a href="<?php echo get_permalink() ?>">
                <div class="mask"></div>
            </a>
        </div>

        <!--Excerpt-->
      
                <strong> <?php the_category(', '); ?></strong>
     
        <h4 class="mb-3 font-weight-bold dark-grey-text">
		<a href="<?php echo get_permalink() ?>" class=""><strong><?php the_title(); ?></strong></a>
        </h4>
       <?php echo get_the_date(); ?></p>
      <!--<spam> Publicado por: <?php // the_author(); ?></spam>-->
        
    </div>
    <!--Grid column-->

  <?php
  if ($counter % 3 == 0) {
  ?>
  </div>
  <!--Grid row-->
  <!--Grid dynamic row-->
  <div class="row wow fadeIn">
  <?php
  }
  $counter++;
  } // end while
  } // end if
  ?>
  </div>
  <!--Grid row-->

