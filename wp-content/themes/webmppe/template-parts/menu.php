<nav class="navbar navbar-expand-lg">
  <a class="navbar-brand" href="#"><h1 class="logo"><img src="imagenes/ministerio-logo-mppe_100x45.png"></h1></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon fas fa-bars"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Conócenos</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">El ministerio</a>
          <a class="dropdown-item" href="#">Entes Adscritos</a>
          <a class="dropdown-item" href="#">Marco Legal</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Información</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Atención al Ciudadano</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Servicios al Funcionario
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#" >Autogestión RRHH</a>
		  <a class="dropdown-item" href="#" >Trámites</a>
		  <a class="dropdown-item" href="#" >Gescolar</a>
		  <a class="dropdown-item" href="#" >Recaudos Académicos</a>
		  <a class="dropdown-item" href="#" >Contrataciones</a>
		  <a class="dropdown-item" href="#" >SALUDME</a>
		  <a class="dropdown-item" href="#" >Reglamento de Ascenso</a>
		
        </div>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Trámites</a>
      </li>
       <!--<li class="nav-item">
        <a class="nav-link" href="#">Prensa</a>
      </li>-->
    </ul>
  </div>
</nav>