
	<h2><?php single_cat_title(); ?></h2>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <article class="<?php post_class();?>">
          
        <?php the_post_thumbnail(); ?>
        <?php echo get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'alignleft' ) );?>

         <h2 id="post-<?php the_ID(); ?>">
                <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
                <?php the_title(); ?></a></p>
                <?php the_tags(); ?>

               <!-- <p> Categoría:   <?php // the_category(); ?></p>-->
        </h2>

    </article>


<small> Publicado el: <?php the_time('F jS, Y'); ?> Publicado por:  <?php the_author_posts_link(); ?></small>
<?php endwhile; else: ?>
<p><?php _e('Lo siento, no encontre nada para mostrar.'); ?></p>
<?php endif; ?>

