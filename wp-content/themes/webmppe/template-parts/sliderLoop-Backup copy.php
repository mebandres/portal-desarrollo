<?php
$args = array(
  'post_type' => 'post',
  'category_name' => 'marzo2021'
);
$the_query = new WP_Query ( $args ); 
?>

<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <!-- Start WP Loop -->
    <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-slide-to="<?php echo $the_query->current_post; ?>" class="<?php if ( $the_query->current_post == 0 ) : ?>active<?php endif; ?>">

    <?php endwhile; endif; ?></button>
  </div>
   <?php rewind_posts(); ?>
  <div class="carousel-inner">

                <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                      $thumbnail_id   = get_post_thumbnail_id();
                      $thumbnail_url  = wp_get_attachment_image_src( $thumbnail_id, 'full', true );
                      $thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attatchment_image_alt', true );
                ?>
                
              <div class="carousel-item  <?php if ( $the_query->current_post == 0 ) : ?>active<?php endif; ?>">
                  <div class="row">
                     
                        <div class="col-6 centro">
                          <div class="text-center"><!-- -->
                            <a href="<?php the_permalink() ?>"> 
                                <h1 class="text-shadow k911-carousel-title"><?php the_title(); ?></h1>
                            </a>
                            <p class="text-shadow d-none d-sm-block"><?php the_excerpt(); ?></p>
                          </div>
                        </div>
                         <div class="col-6">
                        <?php if ( has_post_thumbnail() ) : ?>
                      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php the_post_thumbnail(''); ?>
                      </a>
                       <?php endif; ?>
                       </div>

                  </div>
              </div>


       <?php endwhile; ?>

       <?php else : ?>

       <?php endif; wp_reset_query();?>


  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev"> <a class="carousel-control-prev" href="<?php previous_posts_link( '< Anterior' ); ?>" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only"><?php previous_posts_link( '< Anterior' ); ?></span>
                </a>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
     <a class="carousel-control-next" href="<?php next_posts_link( 'Siguiente >' ); ?>" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only"><?php next_posts_link( 'Siguiente >' ); ?></span>
                </a>
  </button>
</div>


