<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bd_wordpress2' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-!3u2/Rf2a(XG|5?J(rkHGGP0+FJB*d:f8=d0ucG,u@?0<DEeWh.h3Lezy-kW&+:' );
define( 'SECURE_AUTH_KEY',  '/b{s1oCAEMlM?Bk1gw~.wM3@-Z*>0x+kN6~($P`1J{]Sl&};p8^$U)OFNYC%mOpd' );
define( 'LOGGED_IN_KEY',    '{Fu~ED)67YQTc{o+W}X:w_xC.LOT,bkAy)b;3(Rm|;U!Jcly+j0xJlThR8_|^?{}' );
define( 'NONCE_KEY',        'rZ`${F;Uy{<7Y*T,+ZgRN~<iq6q 7M&JlKky*?};RqY]a!2=AZU9J;vmt`^Z*3dS' );
define( 'AUTH_SALT',        'p&@.<<H(+>r!DNh~E?GzNdcC51M{w/*|tLjY1cp)K?LiAtMp$Wmn)Z1WPV7&JEoi' );
define( 'SECURE_AUTH_SALT', '=$/J;XO*BK|/7;@YWc}qS7F2za4foQ8X}U-c6jz:7VQ2R^OvkHLAnd,6mK8pYJ-3' );
define( 'LOGGED_IN_SALT',   '+9RdHedT6vhgIm,;q>=]dRa_9WSI/ak(?$y87*t3N%q_CpvKcH8=}7@iZJJaKH3<' );
define( 'NONCE_SALT',       ')!}#Z;mxH@*|8<EO^oVjF6*uT^.S:oydnGeM_g>&NufE^+v=BQn]x@Diqw3 bq1,' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
