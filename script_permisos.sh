#/etc/bin
#/*Creado por Jaibol Santaella*/
#/Copiar este script en el directorio Raiz de tu aplicación, cms, página web etc etc (En el directorio  principal de tu página web)

echo 'Hola Estoy cambiando los permisos'

find ./ -type d -exec chmod 755 {} \;

find ./ -type f -name '*.php' -exec chmod 644 {} \;

find ./ -type f -name '*.html' -exec chmod 644 {} \;

find ./ -type f -name '*.css' -exec chmod 644 {} \;

find ./ -type f -name '*.js' -exec chmod 644 {} \;

find ./ -type f -name '*.txt' -exec chmod 644 {} \;

find ./ -type f -name '*.jpg' -exec chmod 644 {} \;

find ./ -type f -name '*.png' -exec chmod 644 {} \;

find ./ -type f -name '*.jpeg' -exec chmod 644 {} \;

find ./ -type f -name '*.pdf' -exec chmod 644 {} \;

find ./ -type f -name '*.sql' -exec chmod 644 {} \;

find ./ -type f -name '*.doc' -exec chmod 644 {} \;

find ./ -type f -name '*.ppt' -exec chmod 644 {} \;

find ./ -type f -name '*.ttf' -exec chmod 644 {} \;

find ./ -type f -name '*.woff' -exec chmod 644 {} \;

find ./ -type f -name '*.woff2' -exec chmod 644 {} \;

find ./ -type f -name '*.less' -exec chmod 644 {} \;

find ./ -type f -name '*.map' -exec chmod 644 {} \;

find ./ -type f -name '*.scss' -exec chmod 644 {} \;

find ./ -type f -name '*.svg' -exec chmod 644 {} \;

find ./ -type f -name '*.eot' -exec chmod 644 {} \;

echo 'Cambiando Propietario'

chown -R www-data:www-data *

echo 'Listo ya he terminado'

exit
